from rest_framework import serializers
from bk.models import Saver, Control, SolarExperiment, SolarResult


class SaverSerializer(serializers.ModelSerializer):
    preset = serializers.JSONField()

    class Meta:
        model = Saver
        fields = ('saver_name', 'preset')


class ControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = ('port', 'baudrate', 'on_off', 'sense', 'loc_rem', 'type', 'mode', 'cc_current', 'cv_voltage',
                  'cw_power', 'cr_resistance', 'max_current', 'max_voltage', 'max_power', 'power_density', 'dim_power',
                  'area_of_cell', 'dim_area', 'min_value', 'factor', 'quantity_of_dots', 'time_sleep')


class SolarExpSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolarExperiment
        fields = ('solar_exp_name',)


class SolarResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolarResult
        fields = ('result_name', 'data_for_graph')
