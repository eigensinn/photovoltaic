from django.db import models
import datetime
from django.utils.translation import ugettext_lazy as _
from jsonfield.fields import JSONField


class Saver(models.Model):
    saver_name = models.CharField(max_length=200, verbose_name=_('Saver name'))
    preset = JSONField(default='{}', verbose_name=_('preset'))


class SolarExperiment(models.Model):
    solar_exp_name = models.CharField(max_length=200, verbose_name=_('experiment name'))

    def __unicode__(self):
        return self.solar_exp_name


class SolarResult(models.Model):
    results = models.ForeignKey(SolarExperiment, models.CASCADE)
    result_name = models.CharField(max_length=200, verbose_name=_('result name'))
    product_information = JSONField(verbose_name=_('product information'))
    date = JSONField(verbose_name=_('date'))
    area = JSONField(verbose_name=_('area'))
    efficiency = models.FloatField(verbose_name=_('Efficiency'))
    graph = models.ImageField(upload_to='Solar/%Y/%m/%d',
                              height_field='graph_height',
                              width_field='graph_width',
                              blank=True,
                              null=True,
                              verbose_name=_('image'))
    graph_width = models.IntegerField(blank=True, null=True, verbose_name=_('width'))
    graph_height = models.IntegerField(blank=True, null=True, verbose_name=_('height'))
    data = models.CharField(max_length=100000, verbose_name=_('Data'))
    data_for_graph = JSONField(verbose_name=_('Data for Graph'))
    max_power_point = JSONField(verbose_name=_('Max Power Point'))


class Control(models.Model):
    port = models.IntegerField(default=4)
    baudrate = models.IntegerField(default=9600)
    on_off = models.BooleanField(default=False)
    sense = models.BooleanField(default=False)
    loc_rem = models.BooleanField(default=False)
    type = models.CharField(max_length=50, default='on_off')
    mode = models.CharField(max_length=2, default='cv')
    cc_current = models.FloatField(default=0.0)
    cv_voltage = models.FloatField(default=0.0)
    cw_power = models.FloatField(default=0.0)
    cr_resistance = models.FloatField(default=0.0)
    max_current = models.FloatField(default=0.0)
    max_voltage = models.FloatField(default=0.0)
    max_power = models.FloatField(default=0.0)
    power_density = models.FloatField(default=0.0)
    dim_power = models.CharField(max_length=10, default='W/cm2')
    area_of_cell = models.FloatField(default=0.0)
    dim_area = models.CharField(max_length=10, default='cm2')
    min_value = models.FloatField(default=0.1)
    factor = models.FloatField(default=0.1)
    quantity_of_dots = models.IntegerField(default=50)
    time_sleep = models.FloatField(default=0.1)


class Cell(models.Model):
    cell_name = models.CharField(max_length=200, verbose_name=_('name'))
    note = models.TextField(max_length=500, blank=True, null=True, verbose_name=_('note'))
    add_date = models.DateTimeField(default=datetime.datetime.now, verbose_name=_('add date'))
    CVC = models.ManyToManyField(SolarExperiment, related_name='experiments', verbose_name=_('CVC'))


class Base(models.Model):
    cell_images = models.ForeignKey(Cell, models.CASCADE)
    image_name = models.CharField(max_length=200, verbose_name=_('image name'))
    image = models.ImageField(upload_to='Images/%Y/%m/%d',
                              height_field='ImageHeight',
                              width_field='ImageWidth',
                              blank=True, null=True, verbose_name=_('image'))
    ImageWidth = models.IntegerField(blank=True, null=True, verbose_name=_('width'))
    ImageHeight = models.IntegerField(blank=True, null=True, verbose_name=_('height'))


class Photo(Base):
    class Meta:
        verbose_name = _('photo')
        verbose_name_plural = _('photos')


class Binocular(Base):
    class Meta:
        verbose_name = _('binocular')
        verbose_name_plural = _('binoculars')


class Olympus(Base):
    class Meta:
        verbose_name = _('olympus')
        verbose_name_plural = _('olympuses')


class Scan(Base):
    data = models.FileField(upload_to='Data/%Y/%m/%d',
                            blank=True, null=True, verbose_name=_('data'))
    metadata = models.FileField(upload_to='Data/%Y/%m/%d',
                                blank=True, null=True, verbose_name=_('metadata'))

    class Meta:
        verbose_name = _('scan')
        verbose_name_plural = _('scans')


class REM(Base):
    class Meta:
        verbose_name = _('REM')
        verbose_name_plural = _('REMs')


class ISI(Base):
    class Meta:
        verbose_name = _('ISI')
        verbose_name_plural = _('ISIs')
