import time
import dcload
import json
from django.http import HttpResponse, HttpResponseForbidden
from bk.models import SolarExperiment, SolarResult
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext as _
from django.conf import settings
from serial.serialutil import SerialException
from rest_framework import status, viewsets
from bk.models import Saver, Control
from bk.serializers import SaverSerializer, ControlSerializer, SolarExpSerializer, SolarResultSerializer
from rest_framework.response import Response
from rest_framework.decorators import action
from django.core import serializers as django_serializers


def initializing(port, baudrate):
    load = dcload.DCLoad()
    try:
        load.Initialize("COM" + str(port), baudrate)
    except SerialException:
        for port in range(256):
            try:
                load.Initialize("COM" + str(port), baudrate)
            except SerialException:
                return {"load": None}
            else:
                return {"load": load, "port": port}
    else:
        return {"load": load, "port": port}


def run(request):
    if not request.is_ajax() or not request.method == "POST":
        return HttpResponseForbidden()

    dic = json.loads(request.body)
    baudrate = dic["baudrate"]
    mode = dic["mode"]
    initd = initializing(dic["port"], baudrate)
    load = initd["load"]
    load.SetRemoteControl()
    load.SetMode(mode)
    load.TurnLoadOn()
    # РАСЧЕТНЫЙ БЛОК
    # Получение необходимых параметров
    j = int(dic["vars"]["quantity_of_dots"]) + 1
    ts = float(dic["vars"]["time_sleep"])
    cf = float(dic["vars"]["factor"])
    iq = float(dic["vars"]["power_density"])
    area = float(dic["vars"]["area_of_cell"])
    min_value = float(dic["vars"]["min_value"])

    # Генерация последовательности токов/напряжений
    for fac in range(1, j):
        # Шаг x множитель (порядковый номер точки)
        i = min_value + cf * fac
        # Установка значения постоянного тока/напряжения в точке
        if mode == "cc":
            load.SetCCCurrent(i)
            time.sleep(ts)
        elif mode == "cv":
            load.SetCVVoltage(i)
            time.sleep(ts)
        # Определение и помещение в список выходных значений напряжения/тока/мощности
        values = load.GetInputValues()
        print(values)
        split_values = values.split(",")
    # Определение файла как объекта и объявление списков
    list_a, list_g, list_t, values_i, values_u, values_w, values_q = [], [], [], [], [], [], []
    dim_power = dic["vars"]["dim"]
    dim_area = dic["vars"]["dim_area"]
    # Чтение значений из списка
    for n in range(1, j):
        voltage = float(split_values[0])
        current = float(split_values[1])
        power = float(split_values[2])

        if dim_power == "mW/cm2" and dim_area == "cm2":
            q_f1 = 1000 * float(power) / area
        else:
            q_f1 = float(power) / area

        q = str(q_f1)
        op_state = split_values[3]
        demand_state = split_values[4]
        v2 = voltage.replace('.', ',')
        c2 = current.replace('.', ',')
        p2 = power.replace('.', ',')
        q2 = q.replace('.', ',')
        iq2 = str(iq).replace('.', ',')
        # values_u, values_i используются для построения графиков
        values_u.append(float(voltage))
        values_i.append(float(current))
        values_w.append(float(power))
        values_q.append(q_f1)
        list_a.append([float(voltage), float(current), float(power), float(q), dim_power, str(op_state),
                       str(demand_state)])
        list_g.append([float(current), float(voltage)])
        list_t.append((v2, c2, p2, q2))
    # Расчет оптимальной точки для добавления аннотации на графике
    index_mx_w = values_w.index(max(values_w))
    mx_i = values_i[index_mx_w]
    mx_u = values_u[index_mx_w]

    # Форматирование и запись данных из массивов в файл
    mass_3 = (str(dim_power), str(iq2))
    ar_exp = str(area).replace('.', ',')
    mass_4 = (str(dim_area), str(ar_exp))
    tm = []
    mx_q = max(values_q)
    eff = mx_q / iq
    path = settings.MEDIA_ROOT + "lis.csv"

    # Получение информации об электронной нагрузке
    prod_info = load.GetProductInformation()

    # Получение даты снятия ВАХ
    time_now = load.TimeNow()
    for value in time_now.split(" "):
        tm.append(value)
    with open(path, "a") as lis:
        print('"%s","%s","%s"' % prod_info, file=lis)
        print('"%s","%s","%s","%s","%s"' % time[0], time[1], time[2], time[3], time[4], file=lis)
        print('"S, %s","%s"' % mass_3, file=lis)
        print('"q_inp, %s","%s"' % mass_4, file=lis)
        print('"eff","%s"' % eff, file=lis)
        print('"U, V","I, A","P, W","q, %s"' % dim_power, file=lis)
        for l in range(1, j):
            print('"%s","%s","%s","%s"' % (list_t[l - 1]), file=lis)
        print("", file=lis)

    # БЛОК ОСТАНОВКИ
    load.TurnLoadOff()

    solar_exp_name = dic["solar_exp_name"]
    try:
        SolarExperiment.objects.get(solar_exp_name=solar_exp_name)
    except Exception as ex:
        template = "run_1: {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
        exp = SolarExperiment(solar_exp_name=solar_exp_name)
        exp.save()
    else:
        exp = SolarExperiment.objects.get(solar_exp_name=solar_exp_name)
    try:
        const = 0
        result_name = solar_exp_name + "_" + str(const)
        SolarResult.objects.get(result_name=result_name)
    except Exception as ex:
        template = "run_2: {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
        res = SolarResult(
            results=exp,
            result_name=result_name,
            product_information=json.dumps({"model": prod_info[0], "s/n": prod_info[2], "firmware": prod_info[1]}),
            date=json.dumps({"weekday": tm[0], "month": tm[1], "day": tm[2], "time": tm[3], "year": tm[4]}),
            area=json.dumps({"area": area, "dim_area": dim_area}),
            efficiency=eff,
            data=str(list_t),
            data_for_graph=json.dumps(list_g),
            max_power_point=json.dumps({"max_current": mx_i, "max_voltage": mx_u}))
        res.save(force_insert=True)
    else:
        const = 1
        f = 1
        while f > 0:
            try:
                result_name = solar_exp_name + "_" + str(const)
                SolarResult.objects.get(result_name=result_name)
            except Exception as ex:
                template = "run_3: {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                f = 0
                res = SolarResult(
                    results=exp,
                    result_name=result_name,
                    product_information=json.dumps({"model": prod_info[0], "s/n": prod_info[2],
                                                    "firmware": prod_info[1]}),
                    date=json.dumps({"weekday": tm[0], "month": tm[1], "day": tm[2], "time": tm[3], "year": tm[4]}),
                    area=json.dumps({"area": area, "dim_area": dim_area}),
                    efficiency=eff,
                    data=str(list_t),
                    data_for_graph=str(list_g),
                    max_power_point=json.dumps({"max_current": mx_i, "max_voltage": mx_u}))
                res.save(force_insert=True)
            else:
                const += 1
    return HttpResponse(json.dumps(list_a))


class ControlViewSet(viewsets.ModelViewSet):
    queryset = Control.objects.all()
    serializer_class = ControlSerializer

    @action(detail=False, methods=['post'])
    def set_control(self, request, pk=1):
        serializer = ControlSerializer(data=request.data)
        if serializer.is_valid():
            type = serializer.data['type']
            '''port = serializer.data['port']
            baudrate = serializer.data['baudrate']
            initial = initializing(port, baudrate)
            load = initial["load"]
            if load is not None:
                try:
                    control = Control.objects.get(id=1)
                except ObjectDoesNotExist:
                    c = Control(id=1)
                    c.save()
                    control = Control.objects.get(id=1)
                if type == 'on_off':
                    if serializer.data['on_off']:
                        load.TurnLoadOn()
                    else:
                        load.TurnLoadOff()
                    control.on_off = serializer.data['on_off']
                    control.save(update_fields=['on_off'])
                    return Response(serializer.data['on_off'])
                elif type == 'sense':
                    load.SetRemoteSense(serializer.data["sense"])
                    control.sense = serializer.data['sense']
                    control.save(update_fields=['sense'])
                    return Response(serializer.data['sense'])
                elif type == 'loc_rem':
                    if serializer.data['loc_rem']:
                        load.SetRemoteControl()
                    else:
                        load.SetLocalControl()
                    control.sense = serializer.data['loc_rem']
                    control.save(update_fields=['loc_rem'])
                    return Response(serializer.data['loc_rem'])
                elif type == 'mode':
                    load.SetMode(serializer.data['mode'])
                    control.mode = serializer.data['mode']
                    control.save(update_fields=['mode'])
                    return Response(serializer.data['mode'])
                elif type == 'cc_current':
                    try:
                        load.SetCCCurrent(serializer.data['cc_current'])
                    except Exception as ex:
                        template = "set_cc_current: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("Error: max current is {0} А".format(load.GetMaxCurrent()),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        values = load.GetInputValues()
                        return Response(values["current"])
                elif type == 'cv_voltage':
                    try:
                        load.SetCVVoltage(serializer.data['cv_voltage'])
                    except Exception as ex:
                        template = "set_cv_voltage: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("Error: max voltage is {0} V".format(load.GetMaxVoltage()),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        values = load.GetInputValues()
                        return Response(values["voltage"])
                elif type == 'cw_power':
                    try:
                        load.SetCWPower(serializer.data['cw_power'])
                    except Exception as ex:
                        template = "set_cw_power: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("Error: max power is {0} W".format(load.GetMaxPower()),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        values = load.GetInputValues()
                        return Response(values["power"])
                elif type == 'cr_resistance':
                    try:
                        load.SetCRResistance(serializer.data['cr_resistance'])
                    except Exception as ex:
                        template = "set_cr_resistance: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("error: CR resistance = {0} Ohm exceeds the limit".
                                        format(serializer.data['cr_resistance']),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        values = load.GetInputValues()
                        return Response(values)
                elif type == 'max_current':
                    try:
                        load.SetMaxCurrent(serializer.data['max_current'])
                    except Exception as ex:
                        template = "max_current: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("error: Max current = {0} A exceeds the limit".
                                        format(serializer.data['max_current']),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response(serializer.data['max_current'])
                elif type == 'max_voltage':
                    try:
                        load.SetMaxVoltage(serializer.data['max_voltage'])
                    except Exception as ex:
                        template = "max_voltage: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("error: Max voltage = {0} V exceeds the limit".
                                        format(serializer.data['max_voltage']),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response(serializer.data['max_voltage'])
                elif type == 'max_power':
                    try:
                        load.SetMaxPower(serializer.data['max_power'])
                    except Exception as ex:
                        template = "max_power: {0} occurred. Arguments:\n{1!r}"
                        message = template.format(type(ex).__name__, ex.args)
                        print(message)
                        return Response("error: Max power = {0} W exceeds the limit".
                                        format(serializer.data['max_power']),
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response(serializer.data['max_power'])
            else:
                return Response("Unable to connect to serial port", status=status.HTTP_400_BAD_REQUEST)'''
            # Временная заглушка
            try:
                control = Control.objects.get(id=1)
            except ObjectDoesNotExist:
                c = Control(id=1)
                c.save()
                control = Control.objects.get(id=1)
            setattr(control, type, serializer.data[type])
            control.save(update_fields=[type])
            if type == 'dim_power':
              setattr(control, 'dim_area', serializer.data['dim_area'])
              control.save(update_fields=['dim_area'])
            elif type == 'dim_area':
              setattr(control, 'dim_power', serializer.data['dim_power'])
              control.save(update_fields=['dim_power'])
            return Response(serializer.data[type])
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_control_states(self, request, pk=1):
        try:
            c = Control.objects.get(pk=1)
        except ObjectDoesNotExist:
            return Response({"on_off": False, "sense": False, "loc_rem": False}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(django_serializers.serialize('json', [c]))


class SaverViewSet(viewsets.ModelViewSet):
    queryset = Saver.objects.all()
    serializer_class = SaverSerializer

    @action(detail=False, methods=['post'])
    def save_saver(self, request, pk=None):
        serializer = SaverSerializer(data=request.data)
        if serializer.is_valid():
            saver_name = serializer.data['saver_name']
            try:
                Saver.objects.get(saver_name=saver_name)
            except ObjectDoesNotExist:
                c = Control.objects.get(id=1)
                s = Saver(saver_name=saver_name, preset=django_serializers.serialize('json', [c]))
                s.save()
                return Response(saver_name, status=status.HTTP_201_CREATED)
            else:
                return Response(_("Saver name `%(name)s` is already in use") % {'name': saver_name},
                                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def load_saver(self, request, pk=None):
        serializer = SaverSerializer(data=request.data)
        if serializer.is_valid():
            saver_name = serializer.data['saver_name']
            s = Saver.objects.get(saver_name=saver_name)
            return Response(s.preset)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_saver_list(self, request, pk=None):
        saver_list = Saver.objects.values_list("saver_name", flat=True)
        return Response(saver_list)


class SolarExpViewSet(viewsets.ModelViewSet):
    queryset = SolarExperiment.objects.all()
    serializer_class = SolarExpSerializer

    @action(detail=False, methods=['post'])
    def create_exp(self, request, pk=None):
        serializer = SolarExpSerializer(data=request.data)
        if serializer.is_valid():
            solar_exp_name = serializer.data['solar_exp_name']
            try:
                SolarExperiment.objects.get(solar_exp_name=solar_exp_name)
            except ObjectDoesNotExist:
                s = SolarExperiment(solar_exp_name=solar_exp_name)
                s.save()
                return Response(solar_exp_name, status=status.HTTP_201_CREATED)
            else:
                return Response(_("SolarExperiment name `%(name)s` is already in use") % {'name': solar_exp_name},
                                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def get_exp_list(self, request, pk=None):
        l_ses = {}
        ses = SolarExperiment.objects.values_list("id", flat=True)
        for se in ses:
            rns = list(SolarResult.objects.filter(results_id=se).values_list("result_name", flat=True))
            se_name = SolarExperiment.objects.get(id=se).solar_exp_name
            l_ses[se_name] = rns
        return Response(l_ses)

    @action(detail=False, methods=['post'])
    def get_experiment(self, request, pk=None):
        serializer = SolarExpSerializer(data=request.data)
        if serializer.is_valid():
            solar_exp_name = serializer.data['solar_exp_name']
            s = SolarExperiment.objects.get(solar_exp_name=solar_exp_name)
            results = SolarResult.objects.filter(results__id=s.id)
            results_list = list(results.values_list('result_name', 'data_for_graph'))
            chartkick_list = []
            for result_name, data_for_graph in results_list:
                chartkick_list.append({'name': result_name, 'data': json.loads(json.loads(data_for_graph))})
            return Response(json.dumps(chartkick_list))
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SolarResultViewSet(viewsets.ModelViewSet):
    queryset = SolarExperiment.objects.all()
    serializer_class = SolarExpSerializer

    @action(detail=False, methods=['post'])
    def get_result(self, request, pk=None):
        serializer = SolarResultSerializer(data=request.data)
        if serializer.is_valid():
            result_name = serializer.data['result_name']
            r = SolarResult.objects.get(result_name=result_name)
            return Response(r.data_for_graph)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
