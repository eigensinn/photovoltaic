import json
from bk.models import Cell, Base, Photo, Binocular, Olympus, Scan, REM, ISI, Saver, SolarExperiment, SolarResult
from django.contrib import admin
from easy_thumbnails.files import get_thumbnailer
from django.utils.safestring import mark_safe
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import ugettext_lazy as _


class ExpAdminForm(forms.ModelForm):
    experiments = forms.ModelMultipleChoiceField(queryset=Cell.objects.all(),
                                                 required=False,
                                                 widget=FilteredSelectMultiple(verbose_name=_('experiments'),
                                                                               is_stacked=False))

    class Meta:
        model = SolarExperiment
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(ExpAdminForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['experiments'].initial = self.instance.experiments.all()

    def save(self, commit=True):
        solar_exp = super(ExpAdminForm, self).save(commit=False)

        if commit:
            solar_exp.save()

        if solar_exp.pk:
            solar_exp.experiments = self.cleaned_data['experiments']
            self.save_m2m()

        return solar_exp


class AdminImageWidget(forms.FileInput):
    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        output = []
        try:
            if value and hasattr(value, "url"):
                thumbnailer = get_thumbnailer(value)
                thumbnail_options = {'crop': True}
                thumbnail_options.update({'size': (120, 120)})
                thumb = thumbnailer.get_thumbnail(thumbnail_options)
                output.append(('<a class="boxer" href="%s"><img src="%s"></a>' % (value.url, thumb.url)))
                output.append(super(AdminImageWidget, self).render(name, value, attrs))
        except Exception as ex:
            template = "AdminImageWidget: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
        return mark_safe(''.join(output))


class FlattenJSONWidget(forms.Textarea):
    def render(self, name, value, attrs=None, renderer=None):
        print(name)
        try:
            if value is not None:
                parsed_val = ''
                value = json.loads(value)
                for k, v in value.items():
                    parsed_val += " : ".join([str(k), str(v)])
                    parsed_val += "\n"
                value = parsed_val
        except Exception as ex:
            template = "AdminImageWidget: {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
        return super(FlattenJSONWidget, self).render(name, value, attrs)


class DataWidget(forms.Textarea):
    def render(self, name, value, attrs=None, renderer=None):
        if value is not None:
            parsed_val = ''
            for i in value.replace("[(", "").replace(")]", "").split("("):
                parsed_val += i.replace(")", "")
                parsed_val += "\n"
            value = parsed_val
        return super(DataWidget, self).render(name, value, attrs)


class GraphDataWidget(forms.Textarea):
    def render(self, name, value, attrs=None, renderer=None):
        if value is not None:
            parsed_val = ''
            for i in value.replace("[[", "").replace("]]", "").split("["):
                parsed_val += i.replace("]", "")
                parsed_val += "\n"
            value = parsed_val
        return super(GraphDataWidget, self).render(name, value, attrs)


class ImageForm(forms.ModelForm):
    class Meta:
        model = Base
        widgets = {
            'image': AdminImageWidget,
        }
        fields = "__all__"


class SolarResultGraphForm(forms.ModelForm):
    class Meta:
        model = SolarResult
        widgets = {
            'graph': AdminImageWidget,
            'data': DataWidget,
            'data_for_graph': GraphDataWidget,
            'product_information': FlattenJSONWidget,
            'date': FlattenJSONWidget,
            'area': FlattenJSONWidget,
            'max_power_point': FlattenJSONWidget,
        }
        fields = "__all__"


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class BinocularInline(admin.TabularInline):
    model = Binocular
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class OlympusInline(admin.TabularInline):
    model = Olympus
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class ScanInline(admin.TabularInline):
    model = Scan
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class REMInline(admin.TabularInline):
    model = REM
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class ISIInline(admin.TabularInline):
    model = ISI
    extra = 1
    readonly_fields = ['ImageWidth', 'ImageHeight']
    form = ImageForm


class SolarResultInline(admin.TabularInline):
    model = SolarResult
    extra = 1
    readonly_fields = ['graph_width', 'graph_height']
    form = SolarResultGraphForm


class SolarExperimentAdmin(admin.ModelAdmin):
    inlines = [SolarResultInline]
    list_display = ['id', 'solar_exp_name']
    list_display_links = ('solar_exp_name',)
    search_fields = ['solar_exp_name']


class SolarResultAdmin(admin.ModelAdmin):
    list_display = ['results', 'result_name', 'product_information', 'date', 'area', 'efficiency',
                    'graph', 'graph_width', 'graph_height', 'data', 'data_for_graph', 'max_power_point']
    readonly_fields = ['graph_width', 'graph_height']
    form = ImageForm


class CellAdmin(admin.ModelAdmin):
    inlines = [PhotoInline, BinocularInline, OlympusInline, ScanInline, REMInline, ISIInline]
    list_display = ['cell_name', 'note', 'add_date']
    search_fields = ['cell_name', 'note', 'add_date']
    filter_horizontal = ('CVC',)


class SaverAdmin(admin.ModelAdmin):
    list_display = ['saver_name', 'preset']


admin.site.register(Cell, CellAdmin)
admin.site.register(SolarExperiment, SolarExperimentAdmin)
admin.site.register(Saver, SaverAdmin)
