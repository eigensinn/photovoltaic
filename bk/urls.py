from django.conf.urls import url
from bk import views as bk_views
from rest_framework import routers
from .views import SaverViewSet, ControlViewSet, SolarExpViewSet, SolarResultViewSet


router = routers.DefaultRouter()
router.register(r'api/v1', SaverViewSet)
router.register(r'api/v1/controls', ControlViewSet)
router.register(r'api/v1/experiments', SolarExpViewSet)
router.register(r'api/v1/results', SolarResultViewSet)


urlpatterns = [
    url(r'^run/', bk_views.run)
]

urlpatterns += router.urls
