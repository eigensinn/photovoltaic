# Generated by Django 2.1.7 on 2019-04-07 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bk', '0004_auto_20190405_1534'),
    ]

    operations = [
        migrations.AddField(
            model_name='control',
            name='baudrate',
            field=models.IntegerField(default=9600),
        ),
        migrations.AddField(
            model_name='control',
            name='port',
            field=models.IntegerField(default=4),
        ),
    ]
