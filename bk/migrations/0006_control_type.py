# Generated by Django 2.1.7 on 2019-04-07 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bk', '0005_auto_20190407_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='control',
            name='type',
            field=models.CharField(default='on_off', max_length=50),
        ),
    ]
