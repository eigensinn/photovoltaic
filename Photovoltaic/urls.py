from django.contrib import admin
from django.conf.urls import include, url
from Photovoltaic import views as photovoltaic_views

admin.autodiscover()

urlpatterns = [
    url('^$', photovoltaic_views.homepage),
    url(r'^admin/', admin.site.urls),
    url(r'^saver/(?P<saver_name>\w+)/$', photovoltaic_views.SaverListView.as_view()),
    url(r'^bk/', include('bk.urls')),
]
