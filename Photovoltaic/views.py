from django.views.generic import ListView
from bk.models import Saver, SolarExperiment, SolarResult
from django.shortcuts import get_object_or_404
from django.shortcuts import render


def homepage(request):
    return render(request, 'homepage.html')


class SaverListView(ListView):
    context_object_name = "saver_list"
    template_name = "homepage.html"

    def get_queryset(self):
        s = Saver.objects.all()
        return s
    
    def get_context_data(self, **kwargs):
        context = super(SaverListView, self).get_context_data(**kwargs)
        context['current_saver'] = get_object_or_404(Saver, saver_name=self.kwargs['saver_name'])
        l_ses = []
        ses = SolarExperiment.objects.values_list("id", flat=True)
        for se in ses:
            l_rns = []
            rns = SolarResult.objects.filter(results_id=se).values_list("result_name", flat=True)
            for rn in rns:
                li = '<li><a class="result" href="#">'+rn+'</a></li>'
                l_rns.append(li)
            s_rns = "".join(l_rns)
            se_name = SolarExperiment.objects.get(id=se).solarexp_name
            s_se = '<a href="#" class="experiment">'+se_name+'</a><ul>'+s_rns+'</ul>'
            l_ses.append(s_se)
        context['solar_results'] = l_ses
        return context
