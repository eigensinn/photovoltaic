import Vue from 'vue'
import Vuex from 'vuex'
import { Saver } from '../api/savers'
import { Control } from '../api/controls'
import { Exp, Res } from '../api/experiments'

import {
  SAVE_SAVER, LOAD_SAVER, GET_SAVERS, ON_OFF_CONTROL, SENSE_CONTROL,
  LOC_REM_CONTROL, PORT_CONTROL, BAUD_CONTROL, MODE_CONTROL, CC_CURRENT_CONTROL,
  CV_VOLTAGE_CONTROL, CW_POWER_CONTROL, CR_RESISTANCE_CONTROL,
  MAX_VOLTAGE_CONTROL, MAX_CURRENT_CONTROL, MAX_POWER_CONTROL,
  POWER_DENSITY_CONTROL, DIM_POWER, AREA_CONTROL, DIM_AREA, MIN_VALUE_CONTROL,
  FACTOR_CONTROL, QUANTITY_CONTROL, TIME_SLEEP_CONTROL, GET_CONTROLS, CREATE_EXP,
  GET_EXPERIMENTS, GET_EXPERIMENT, GET_RESULT } from './mutation-types.js'

Vue.use(Vuex)

// Состояние
const state = {
  savers: [], // список сохранений
  controls: {},
  experiments: {},
  result_data: [],
  exp_data: []
}

// Геттеры
const getters = {
  savers: state => state.savers, // получаем список пресетов
  controls: state => state.controls, // получаем состояния элементов управления
  experiments: state => state.experiments,
  result_data: state => state.result_data,
  exp_data: state => state.exp_data
}

// Мутации
const mutations = {
  // Добавляем сохранение в список
  SAVE_SAVER: (state, saver) => {
    state.savers = [saver, ...state.savers]
  },
  // Загружаем сохранение
  LOAD_SAVER: (state, fields) => {
    state.controls = fields
  },
  // Задаем список сохранений
  [GET_SAVERS] (state, { savers }) {
    state.savers = savers
  },
  // Запоминаем состояние on_off
  ON_OFF_CONTROL: (state, payload) => {
    state.controls.on_off = payload
  },
  // Запоминаем состояние sense
  SENSE_CONTROL: (state, payload) => {
    state.controls.sense = payload
  },
  // Запоминаем состояние loc_rem
  LOC_REM_CONTROL: (state, payload) => {
    state.controls.loc_rem = payload
  },
  PORT_CONTROL: (state, payload) => {
    state.controls.port = payload
  },
  BAUD_CONTROL: (state, payload) => {
    state.controls.baudrate = payload
  },
  MODE_CONTROL: (state, payload) => {
    state.controls.mode = payload
  },
  CC_CURRENT_CONTROL: (state, payload) => {
    state.controls.cc_current = payload
  },
  CV_VOLTAGE_CONTROL: (state, payload) => {
    state.controls.cv_voltage = payload
  },
  CW_POWER_CONTROL: (state, payload) => {
    state.controls.cw_power = payload
  },
  CR_RESISTANCE_CONTROL: (state, payload) => {
    state.controls.cr_resistance = payload
  },
  MAX_CURRENT_CONTROL: (state, payload) => {
    state.controls.max_current = payload
  },
  MAX_VOLTAGE_CONTROL: (state, payload) => {
    state.controls.max_voltage = payload
  },
  MAX_POWER_CONTROL: (state, payload) => {
    state.controls.max_power = payload
  },
  POWER_DENSITY_CONTROL: (state, payload) => {
    state.controls.power_density = payload
  },
  DIM_POWER: (state, payload) => {
    state.controls.power_density_unit = payload
  },
  AREA_CONTROL: (state, payload) => {
    state.controls.area_of_cell = payload
  },
  DIM_AREA: (state, payload) => {
    state.controls.area_of_cell_unit = payload
  },
  MIN_VALUE_CONTROL: (state, payload) => {
    state.controls.min_value = payload
  },
  FACTOR_CONTROL: (state, payload) => {
    state.controls.factor = payload
  },
  QUANTITY_CONTROL: (state, payload) => {
    state.controls.quantity_of_dots = payload
  },
  TIME_SLEEP_CONTROL: (state, payload) => {
    state.controls.time_sleep = payload
  },
  // Задаем список состояний элементов управления
  [GET_CONTROLS] (state, { fields }) {
    state.controls = fields
  },
  CREATE_EXP: (state, expName) => {
    state.experiments[expName] = []
  },
  [GET_EXPERIMENTS] (state, { experiments }) {
    state.experiments = experiments
  },
  GET_EXPERIMENT: (state, data) => {
    state.exp_data = data
  },
  GET_RESULT: (state, data) => {
    state.result_data = data
  }
}

// Действия
const actions = {
  saveSaver ({ commit }, jsonstring) {
    Saver.save(jsonstring).then(saver => {
      commit(SAVE_SAVER, saver)
    })
  },
  loadSaver ({ commit }, saverName) {
    Saver.load(saverName).then(controls => {
      let fields = JSON.parse(controls)[0].fields
      commit(LOAD_SAVER, fields)
    })
  },
  getSavers ({ commit }) {
    Saver.list().then(savers => {
      commit(GET_SAVERS, { savers })
    })
  },
  createExp ({ commit }, expName) {
    Exp.create(expName).then(expName => {
      commit(CREATE_EXP, expName)
    })
  },
  getExperiments ({ commit }) {
    Exp.exp_list().then(experiments => {
      commit(GET_EXPERIMENTS, { experiments })
    })
  },
  getExperiment ({ commit }, expName) {
    Exp.get_experiment(expName).then(results => {
      let data = JSON.parse(results)
      commit(GET_EXPERIMENT, data)
    })
  },
  getResult ({ commit }, resultName) {
    Res.get_result(resultName).then(result => {
      let data = JSON.parse(result)
      commit(GET_RESULT, data)
    })
  },
  setControl ({ commit }, jsonstring) {
    Control.set_control(jsonstring).then(response => {
      switch (jsonstring.type) {
        case 'on_off':
          commit(ON_OFF_CONTROL, response)
          break
        case 'sense':
          commit(SENSE_CONTROL, response)
          break
        case 'loc_rem':
          commit(LOC_REM_CONTROL, response)
          break
        case 'port':
          commit(PORT_CONTROL, response)
          break
        case 'baudrate':
          commit(BAUD_CONTROL, response)
          break
        case 'mode':
          commit(MODE_CONTROL, response)
          break
        case 'cc_current':
          commit(CC_CURRENT_CONTROL, response)
          break
        case 'cv_voltage':
          commit(CV_VOLTAGE_CONTROL, response)
          break
        case 'cw_power':
          commit(CW_POWER_CONTROL, response)
          break
        case 'cr_resistance':
          commit(CR_RESISTANCE_CONTROL, response)
          break
        case 'max_current':
          commit(MAX_CURRENT_CONTROL, response)
          break
        case 'max_voltage':
          commit(MAX_VOLTAGE_CONTROL, response)
          break
        case 'max_power':
          commit(MAX_POWER_CONTROL, response)
          break
        case 'power_density':
          commit(POWER_DENSITY_CONTROL, response)
          break
        case 'dim_power':
          commit(DIM_POWER, response)
          break
        case 'area_of_cell':
          commit(AREA_CONTROL, response)
          break
        case 'dim_area':
          commit(DIM_AREA, response)
          break
        case 'min_value':
          commit(MIN_VALUE_CONTROL, response)
          break
        case 'factor':
          commit(FACTOR_CONTROL, response)
          break
        case 'quantity_of_dots':
          commit(QUANTITY_CONTROL, response)
          break
        case 'time_sleep':
          commit(TIME_SLEEP_CONTROL, response)
          break
      }
    })
  },
  getControls ({ commit }) {
    Control.controls_dict().then(controls => {
      let fields = JSON.parse(controls)[0].fields
      commit(GET_CONTROLS, { fields })
    })
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
