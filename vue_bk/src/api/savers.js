import { HTTP } from './common'

export const Saver = {
  save (jsonstring) {
    return HTTP.post('/save_saver/', jsonstring)
      .then(response => { return response.data })
      .catch(error => { return error })
  },
  load (saver) {
    return HTTP.post('/load_saver/', saver)
      .then(response => { return response.data })
      .catch(error => { return error })
  },
  list () {
    return HTTP.get('/get_saver_list/').then(response => {
      return response.data
    })
  }
}
