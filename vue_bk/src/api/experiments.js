import { HTTP } from './common'

export const Exp = {
  create (expName) {
    return HTTP.post('/experiments/create_exp/', expName)
      .then(response => { return response.data })
      .catch(error => { return error })
  },
  exp_list () {
    return HTTP.get('/experiments/get_exp_list/').then(response => {
      return response.data
    })
  },
  get_experiment (expName) {
    return HTTP.post('/experiments/get_experiment/', expName)
      .then(response => { return response.data })
      .catch(error => { return error })
  }
}

export const Res = {
  get_result (resultName) {
    return HTTP.post('/results/get_result/', resultName)
      .then(response => { return response.data })
      .catch(error => { return error })
  }
}
