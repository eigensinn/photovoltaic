import { HTTP } from './common'

export const Control = {
  set_control (jsonstring) {
    return HTTP.post('/controls/set_control/', jsonstring)
      .then(response => { return response.data })
      .catch(error => { return error })
  },
  run (jsonstring) {
    return HTTP.post('/controls/run/', jsonstring)
      .then(response => { return response.data })
      .catch(error => { return error })
  },
  controls_dict () {
    return HTTP.get('/controls/get_control_states/').then(response => {
      return response.data
    })
  }
}
